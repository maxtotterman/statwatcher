defmodule Statwatcher.APIHelper do
  def format_url(api) do
    case api do
       :youtube -> youtube_api_url()
       :instagram -> instagram_api_url()
       _ -> raise "API not supported"
    end
  end

  defp instagram_api_url do
    uri = "https://www.instagram.com/"
    username = "thefathero"
    fields = "?__a=1"
    "#{uri}#{username}/#{fields}"
  end

  defp youtube_api_url do
    uri = "https://www.googleapis.com/youtube/v3/"
    channel = "id=" <> "UCYjqe77MUKwDzJlqLaI73yw"
    key = "key=" <> "ADD_YOUR_KEY_HERE"
    "#{uri}channels?#{channel}&#{key}&part=statistics"
  end

  def parse(result) do
    case result do
      {:youtube, body} -> parse_youtube_data(body)
      {:instagram, body} -> parse_instagram_data(body)
      _ -> raise "Invalid parse data"
   end
  end

  defp parse_youtube_data(body) do
    now = DateTime.utc_now |> DateTime.to_string
    %{ items: [%{statistics: stats} | _]} = Poison.decode! body, keys: :atoms
    [
      now,
      stats.subscriberCount,
      stats.videoCount,
      stats.viewCount,
      "Youtube"
    ]
  end

  defp parse_instagram_data(body) do
    now = DateTime.utc_now |> DateTime.to_string
    %{ graphql: %{user: data}} = Poison.decode! body, keys: :atoms
    [
      now,
      data.edge_followed_by.count,
      data.edge_owner_to_timeline_media.count,
      nil,
      "Instagram"
    ]
  end
end
