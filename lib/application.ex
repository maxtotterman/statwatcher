defmodule Statwatcher.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [worker(Statwatcher.Scheduler, [])]
    options = [strategy: :one_for_one, name: Statwatcher.Supervisor]

    Supervisor.start_link children, options
  end
end
