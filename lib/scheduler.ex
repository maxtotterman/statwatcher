defmodule Statwatcher.Scheduler do

  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    handle_info(:work, state)
    {:ok, state}
  end

  def handle_info(:work, state) do
    platforms = [:youtube, :instagram]
    Enum.each(platforms, &Statwatcher.run/1)
    schedule_work()
    {:noreply, state}
  end

  def schedule_work do
    Process.send_after(self(), :work, 5 * 1000)
  end
end
