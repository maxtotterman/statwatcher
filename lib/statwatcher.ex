defmodule Statwatcher do
  import Statwatcher.APIHelper

  def run(social_media_platform) do
    IO.puts "asd #{social_media_platform}"
    social_media_platform
      |> fetch_stats
      |> save_csv
  end

  def youtube_column_names do
    Enum.join ~w(Date Subscribers Media Views Platform), ","
  end

  def save_csv(row) do
    filename = "stats.csv"

    unless File.exists? filename do
      File.write! filename, youtube_column_names() <> "\n"
    end

    File.write! filename, row <> "\n" , [:append]
  end

  def fetch_stats(platform) do
    %{ body: body } = HTTPoison.get! format_url(platform)
    parse({platform, body}) |> Enum.join(", ")
  end

end

